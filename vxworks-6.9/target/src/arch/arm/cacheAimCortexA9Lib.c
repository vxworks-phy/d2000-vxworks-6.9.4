/* cacheAimCortexA9Lib.c - AIM CortexA9 cache support library */

/*
 * Copyright (c) 2009-2014, 2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01k,08apr16,cfm  added workaround for ARM errata 764369
01j,26may14,c_l  Remove executable attribute for page table. (VXW6-82668)
01i,17apr14,c_l  add the write protection of L2 page table. (VXW6-73478)
01h,03dec13,c_l  Add CACHE_OP_TEXTUPDATE of the cacheOps. (WIND00440975)
01g,18sep12,rec  WIND00355229 - SMP coherency initialization should not
                 depend on the CACHE_SNOOP_ENABLE
01f,10sep12,m_h  L2 cachePipeFlush (WIND00240234)
01e,12jun12,jdw  Add support to use core specific cached page table attributes,
                 WIND00342202, WIND00342756
01d,19dec11,j_b  support cacheClear for I-cache via invalidate (WIND00084404)
01c,11jan11,j_b  prevent cache clear & disable for GuestOS
01b,19may10,z_l  Use new functions in thisCache[] to support L2 cache.
01a,20oct09,z_l  Created from cacheAimCortexA8Lib.c, rev 01b
*/

/*
DESCRIPTION
This library contains architecture-specific cache library functions for the
Cortex-A and -R family instruction and data caches.  Processor-specific
constraints are addressed in the manual entries for routines in this library.
If the caches are unavailable or uncontrollable, the routines return ERROR.

NOTE
I-cache can be enabled without enabling the MMU. No specific
support for this mode of operation is included.

\INTERNAL
There are some terminology problems. VxWorks uses the following definitions:
Clear = VxWorks "Flush" then VxWorks "Invalidate"
Flush = write out to memory = ARM "clean"
Invalidate = make cache entry invalid = ARM "flush"

\INTERNAL
The cache enable and disable processes consist of the following actions...
To enable a disabled cache, first the cache is fully invalidated.  Then the
cache mode (write-through, copy-back, etc.) is configured.  Finally, the cache
is turned on.  Enabling an already enabled cache results in no operation.

\INTERNAL
To disable an enabled cache, first the cache is invalidated.  However, a cache
configured in copy-back mode must first have been pushed out to memory.  Once
invalidated, the cache is turned off.  Disabling an already disabled cache
results in no operation.

For general information about caching, see the manual entry for cacheLib.

INCLUDE FILES: cacheLib.h, mmuLib.h

SEE ALSO: cacheLib, vmLib,
\tb ARM Architecture Reference Manual ARMv7-A and ARMv7-R edition (ARM DDI 0406)
*/

/* includes */

#include <vxWorks.h>

#include <stdlib.h>

#include <aimCacheLib.h>
#include <arch/arm/cacheCortexA9Lib.h>
#include <arch/arm/cacheArmLib.h>

#ifndef INCLUDE_MEM_ALLOT
#include <memLib.h>
#endif /* !INCLUDE_MEM_ALLOT */

#include <private/memPartLibP.h>
#include <private/vmLibP.h>
#include <private/funcBindP.h>

#ifdef _WRS_CONFIG_ENABLE_CACHED_PAGE_TBL
#include "arch/arm/mmuArmArch6PalLib.h"
#endif /* _WRS_CONFIG_ENABLE_CACHED_PAGE_TBL */

/* External declarations */

#ifdef _WRS_CONFIG_ENABLE_CACHED_PAGE_TBL
extern UINT32 mmuArchTtbrFlags;
extern UINT32 mmuArchPteFlags;
#endif /* _WRS_CONFIG_ENABLE_CACHED_PAGE_TBL */

/* forward declarations */

STATUS cacheAimCortexA9LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);


/*
 * Globals.
 *
 * Those globals which have values assigned to them in
 * cacheAimCortexA9LibInit(), need to be predefined, else they may be put in
 * BSS. Since BSS is not cleared until after cacheAimCortexA9LibInit() has
 * been called, this would be a problem.
 */

#ifdef _WRS_SUPPORT_CACHE_XLATE
FUNCPTR cacheAimCortexA9PhysToVirt;  /* phys to virt addr translation rtn */
FUNCPTR cacheAimCortexA9VirtToPhys;  /* virt to phys addr translation rtn */
#endif /* _WRS_SUPPORT_CACHE_XLATE */

/* define the I-cache features not supported by arch */

#define ICACHE_UNSUPPORT (CACHE_SNOOP_ENABLE | CACHE_SNOOP_DISABLE | \
                          CACHE_BURST_ENABLE | CACHE_BURST_DISABLE)

#ifdef _WRS_CONFIG_SMP
/* for SMP, CACHE_SNOOP_ENABLE is supported */

#   define DCACHE_UNSUPPORT (CACHE_SNOOP_DISABLE | CACHE_BURST_ENABLE | \
                             CACHE_BURST_DISABLE)
#else
#   define DCACHE_UNSUPPORT ICACHE_UNSUPPORT
#endif /* _WRS_CONFIG_SMP */

#ifdef _WRS_CONFIG_ARM_ERRATA_764369
#ifdef _WRS_HAS_DCC_ASM_SYNTAX
__asm volatile void errata764369 (void)
{
! "r0", "r1"
        mrc     p15, 4, r0, c15, c0, 0
        ldr     r1, [r0, #0x30]
        orr     r1, r1, #1
        str     r1, [r0, #0x30]
}
#else  /* _WRS_HAS_GCC_ASM_SYNTAX */
#define errata764369()                      \
    __asm __volatile (                      \
        "mrc     p15, 4, r0, c15, c0, 0\n"  \
        "ldr     r1, [r0, #0x30]\n"         \
        "orr     r1, r1, #1\n"              \
        "str     r1, [r0, #0x30]\n"         \
        ::: "r0", "r1")
#endif /* _WRS_HAS_GCC_ASM_SYNTAX */
#endif /* _WRS_CONFIG_ARM_ERRATA_764369 */

/*
 * Arch-specific interface to AIM cache manager
 *
 * Note that many of the cacheOps for Arch 6 are still valid for Arch 7 and
 * have been brought forward.
 *
 * (Only level 1 is described because there are no cacheOps specific to
 * higher levels of cache.)
 */

LOCAL CACHECONFIG thisCache =
    {
      2, /* nCaches */
      {
       {
        { /* cacheAttr for I Cache (updated at runtime) */
          (CACHETYPES) CACHE_TYPE_I,        /* cacheType */
          (UINT32)     CACHE_WRITETHROUGH,  /* cacheModeDefault */
          (UINT32)     0,                   /* cacheModeSupported */
          (UINT32)     0,                   /* cacheSize */
          (UINT32)     0,                   /* lineSize */
          (UINT32)     0,                   /* lineStep */
          (UINT32)     0,                   /* numWays */
          (UINT32)     0,                   /* wayStep */
          (UINT32)     0,                   /* numSegs */
          (UINT32)     0,                   /* segStep */
          (void *)     0                    /* cacheBase */
        },
        { /* cacheOps */
          { (UINT32) (C_FLG_MGMT | CACHE_OP_ENABLE),
                                        (FUNCPTR) cacheAimArch7IEnable },
          { (UINT32) (C_FLG_MGMT | CACHE_OP_DISABLE),
                                        (FUNCPTR) cacheAimArch7IDisable },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_INVALIDATE),
                                        (FUNCPTR) cacheArchIInvalidate },
          { (UINT32) (C_FLG_VIRT | C_FLG_ALL | CACHE_OP_INVALIDATE),
                                        (FUNCPTR) cacheArchIInvalidateAll },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_CLEAR),
                                        (FUNCPTR) cacheArchIInvalidate },
          { (UINT32) (C_FLG_VIRT | C_FLG_ALL | CACHE_OP_CLEAR),
                                        (FUNCPTR) cacheArchIInvalidateAll },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_ISON),
                                        (FUNCPTR) cacheAimArch7IIsOn },
          { (UINT32) (0 | CACHE_OP_TEXTUPDATE),
                                        (FUNCPTR) cacheAimArch7TextUpdate }
        }
       },
       {
        { /* cacheAttr for D Cache (updated at runtime) */
          (CACHETYPES) CACHE_TYPE_D,    /* cacheType */
          (UINT32)     CACHE_COPYBACK,  /* cacheModeDefault */
          (UINT32)     0,               /* cacheModeSupported */
          (UINT32)     0,               /* cacheSize */
          (UINT32)     0,               /* lineSize */
          (UINT32)     0,               /* lineStep */
          (UINT32)     0,               /* numWays */
          (UINT32)     0,               /* wayStep */
          (UINT32)     0,               /* numSegs */
          (UINT32)     0,               /* segStep */
          (void *)     0                /* cacheBase */
        },
        { /* cacheOps */
          { (UINT32) (C_FLG_MGMT | CACHE_OP_ENABLE),
                                        (FUNCPTR) cacheAimArch7DEnable },
          { (UINT32) (C_FLG_MGMT | CACHE_OP_DISABLE),
                                        (FUNCPTR) cacheAimArch7DDisable },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_FLUSH),
                                        (FUNCPTR) cacheArchDFlush },
          { (UINT32) (C_FLG_VIRT | C_FLG_ALL | CACHE_OP_FLUSH),
                                        (FUNCPTR) cacheArchDFlushAll },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_INVALIDATE),
                                        (FUNCPTR) cacheArchDInvalidate },
          { (UINT32) (C_FLG_VIRT | C_FLG_ALL | CACHE_OP_INVALIDATE),
                                        (FUNCPTR) cacheArchDInvalidateAll },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_CLEAR),
                                        (FUNCPTR) cacheArchDClear },
          { (UINT32) (C_FLG_VIRT | C_FLG_ALL | CACHE_OP_CLEAR),
                                        (FUNCPTR) cacheArchDClearAll },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_PIPEFLUSH),
                                        (FUNCPTR) cacheArchPipeFlush },
#ifdef _WRS_SUPPORT_CACHE_XLATE
          { (UINT32) (C_FLG_VIRT | CACHE_OP_DMAVIRTOPHYS),
                                        (FUNCPTR) NULL },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_DMAPHYSTOVIRT),
                                        (FUNCPTR) NULL },
#endif /* _WRS_SUPPORT_CACHE_XLATE */
#ifndef _WRS_CONFIG_MEM_ALLOT
          { (UINT32) (C_FLG_VIRT | CACHE_OP_DMAMALLOC),
                                        (FUNCPTR) cacheAimArch7DmaMalloc },
          { (UINT32) (C_FLG_VIRT | CACHE_OP_DMAFREE),
                                        (FUNCPTR) cacheAimArch7DmaFree },
#endif /*_WRS_CONFIG_MEM_ALLOT*/
          { (UINT32) (C_FLG_VIRT | CACHE_OP_ISON),
                                        (FUNCPTR) cacheAimArch7DIsOn },
          { (UINT32) (0 | CACHE_OP_TEXTUPDATE),
                                        (FUNCPTR) cacheAimArch7TextUpdate }
        }
       }
      }
    };

/*******************************************************************************
*
* cacheCortexA9LibInstall - install specific CortexA9 cache library
*
* This routine is provided so that a call to this routine selects the
* specific cache library.  It also allows any virtual <-> physical address
* translation routines provided by the BSP to be passed to the cache
* library as parameters rather than as global data.  These are then used by
* cacheDrvVirtToPhys() and cacheDrvPhysToVirt().
*
* If the default address map is such that virtual and physical
* addresses are identical (this is normally the case with BSPs), the
* parameters to this routine can be NULL pointers.  Only where the
* default memory map is such that virtual and physical addresses are
* different, need the translation routines be provided.
*
* If the address map is such that the mapping described within the
* sysPhysMemDesc structure of the BSP is accurate, then the parameters to
* this routine can be mmuPhysToVirt and mmuVirtToPhys: two routines
* provided within the architecture code that perform the conversion based
* on the information within that structure.  If that assumption is not
* true, then the BSP must provide its own translation routines.
*
* RETURNS: N/A
*
* The specification of the translation routines is as follows:
*
* PHYS_ADDR virtToPhys
*     (
*     VIRT_ADDR   virtAddr  /@ virtual addr to be translated @/
*     )
*
* RETURNS: the physical address
*
* VIRT_ADDR physToVirt
*     (
*     PHYS_ADDR   physAddr  /@ physical addr to be translated @/
*     )
*
* RETURNS: the virtual address
*
* The caching capabilities and modes for different cache types vary
* considerably (see below).  The memory map is BSP-specific and some
* functions need knowledge of the memory map, so they have to be provided
* in the BSP.  Moreover, now that chips are being made on a
* "mix-and-match" basis, selection of the cache type is now a BSP issue.
*
* Separate instruction and data L1 caches.  The global mode of the data
* cache cannot be configured and must be copy-back.  Individual pages
* can be marked (using the MMU) as write-through or copy-back but
* separate write-buffer (which is always enabled) is used for all
* cacheable writes (even write-through).  The cache replacement
* algorithm can be set to be random or round-robin in hardware.  No
* reliance on the method should be present in this code, but it has only
* been tested in the default state (random replacement).
*
* INTERNAL
* This routine is called (from sysHwInit0()), before cacheLibInit() has
* been called, before sysHwInit has been called, and before BSS has been
* cleared.
*
*/

void  cacheCortexA9LibInstall
    (
    VIRT_ADDR(physToVirt) (PHYS_ADDR), /* phys to virt addr translation rtn */
    PHYS_ADDR(virtToPhys) (VIRT_ADDR)  /* virt to phys addr translation rtn */
    )
    {
    static BOOL initialized =  FALSE;

    /* protect against being called twice */

    if (initialized)
        return;

#ifdef _WRS_SUPPORT_CACHE_XLATE
    /*
     * Normally, cacheAimArch7DmaMalloc() will return addresses with the
     * same virtual to physical address mapping as the rest of the system
     * (which is normally identical), so the following two routines do
     * not need to be provided.  However, on some systems it is not
     * possible for a memory mapping to be created where virtual and
     * physical addresses are identical.  In that case, the BSP will
     * provide routines to perform this mapping, and we should use
     * them.
     */

    cacheAimCortexA9PhysToVirt = (FUNCPTR) physToVirt;
    cacheAimCortexA9VirtToPhys = (FUNCPTR) virtToPhys;
#endif /* _WRS_SUPPORT_CACHE_XLATE */

    /*
     * Initialize the function pointer so that the
     * architecture-independent code calls the correct code.
     */

    sysCacheLibInit = cacheAimCortexA9LibInit;

    initialized =  TRUE;
    return;
    }

/*******************************************************************************
*
* cacheAimCortexA9LibInit - initialize Cortex-A9 cache library function pointers
*
* This routine initializes the cache library for Cortex-A9 processors.  It
* initializes the function pointers and configures the caches to the
* specified cache modes.  Modes should be set before caching is
* enabled.  If two complementary flags are set (enable/disable), no
* action is taken for any of the input flags.
*
* NOTE
* This routine should not be called directly from cacheLibInit() on the
* new (generic) architectures.  Instead, cacheArchLibInstall() should
* be called.
*
* INTERNAL
* This routine is called (from cacheLibInit()), before sysHwInit has
* been called, and before BSS has been cleared.
*
* RETURNS: OK always
*
*/

STATUS cacheAimCortexA9LibInit
    (
    CACHE_MODE instMode,   /* instruction cache mode */
    CACHE_MODE dataMode    /* data cache mode */
    )
    {
    static BOOL initialized = 0 ;
    int i;
    INT32 opFlags, cacheOp_idx;

    CACHEATTRIBUTES * cacheAttrP;
    CACHEOP * cacheOpP;

    UINT32 cTypes, szInfo, temp, temp2;

    /* protect (silently) against being called twice */

    if (initialized)
        return (OK);

#ifdef _WRS_CONFIG_ARM_ERRATA_764369
     errata764369 ();
#endif /* _WRS_CONFIG_ARM_ERRATA_764369 */

    /*
     * Find out what size(s) and type(s) of cache are fitted at cache level 1.
     * (Only level 1 is queried because there are no cacheOps specific to
     * higher levels of cache.)
     * This must be done before anything else, as we will need the results
     * in order to be able to clean/flush etc.
     * This routine reads coprocessor 15 reg 0.
     */

    cTypes =  cacheAimArch7Identify (ARMV7_CACHE_LVL1);  /* get cache type(s) */

    /* set cache attributes */

    for ( i = 0; i < (UINT32) thisCache.nCaches; i++ )
        {
        cacheAttrP = &thisCache.cachePrimitives[i].cacheAttr;

        if ( (cacheAttrP->cacheType == CACHE_TYPE_I) &&
             ((cTypes & ARMV7_CLID_ICACHE_TYPE) == ARMV7_CLID_ICACHE_TYPE) )
            {
            cacheAttrP->cacheModeDefault = instMode;

            /* get cache info */

            szInfo = cacheAimArch7SizeInfoGet (ARMV7_CACHE_LVL1,
                                               ARMV7_CSSEL_ICACHE_TYPE);

            /* decode cache mode */

            temp = szInfo & ARMV7_CCSID_MODE_MSK;
            if (temp & ARMV7_CCSID_MODE_WT)
                cacheAttrP->cacheModeSupported = CACHE_WRITETHROUGH;

            if (temp & ARMV7_CCSID_MODE_WB)
                cacheAttrP->cacheModeSupported |= CACHE_COPYBACK;

            if (temp & ARMV7_CCSID_MODE_RA)
                cacheAttrP->cacheModeSupported |= CACHE_NO_WRITEALLOCATE;

            if (temp & ARMV7_CCSID_MODE_WA)
                cacheAttrP->cacheModeSupported |= CACHE_WRITEALLOCATE;

        /* 
         * decode cache line size, read as:
         * Log2(Number of words in cache line)-2
         * 
         * line size is needed in bytes, so number is shifted left
         * by 2 additional bits (4 bytes/word)
         *
         * NOTE: If this is to be core-specific, there may be only a
         * very limited number of cache configurations, so the code
         * could be simplified to check for them
         */

        temp = szInfo & ARMV7_CCSID_LINESIZE_MSK;
        cacheAttrP->lineSize = 1 << (temp + 4);

            /* cache line step = line size */

            cacheAttrP->lineStep = cacheAttrP->lineSize;

        /* decode associativity, read as: (# of ways - 1) 
         * The associativity does not have to be a power of 2.
         */

        cacheAttrP->numWays = ((szInfo & ARMV7_CCSID_WAYNUM_MSK) >>
                   ARMV7_CCSID_WAYNUM_SHFT) + 1;

            cacheAttrP->wayStep = cacheAttrP->lineSize;

            /* decode number of sets, read as: (# of sets - 1) 
             * The number of sets does not have to be a power of 2.
             */

            cacheAttrP->numSegs = ((szInfo & ARMV7_CCSID_SETNUM_MSK) >>
                                    ARMV7_CCSID_SETNUM_SHFT) + 1;

            cacheAttrP->segStep = cacheAttrP->lineSize;

        /* 
         * calculate cache size:
         * # sets * # ways * # bytes/line
         */

        cacheAttrP->cacheSize = cacheAttrP->numSegs * 
                    cacheAttrP->numWays *
                    cacheAttrP->lineSize;
            }
        else if ( (cacheAttrP->cacheType == CACHE_TYPE_D) &&
              (((cTypes & ARMV7_CLID_DCACHE_TYPE) == ARMV7_CLID_DCACHE_TYPE) ||
               ((cTypes & ARMV7_CLID_UCACHE_TYPE) == ARMV7_CLID_UCACHE_TYPE)) )
            {

            /* Set the default Data Cache Mode */

            cacheAttrP->cacheModeDefault = dataMode;

            /* set the translation functions (set in sysLib.c) */

            cacheOpP = thisCache.cachePrimitives[i].cacheOps;

            /* get cache info */

            szInfo =  cacheAimArch7SizeInfoGet (ARMV7_CACHE_LVL1,
                         ARMV7_CSSEL_DCACHE_TYPE);

            /* decode cache mode */

            temp = szInfo & ARMV7_CCSID_MODE_MSK;

            if (temp & ARMV7_CCSID_MODE_WT)
                cacheAttrP->cacheModeSupported = CACHE_WRITETHROUGH;

            if (temp & ARMV7_CCSID_MODE_WB)
                cacheAttrP->cacheModeSupported |= CACHE_COPYBACK;

            if (temp & ARMV7_CCSID_MODE_RA)
                cacheAttrP->cacheModeSupported |= CACHE_NO_WRITEALLOCATE;

            if (temp & ARMV7_CCSID_MODE_WA)
                cacheAttrP->cacheModeSupported |= CACHE_WRITEALLOCATE;

        /* 
         * decode cache line size, read as:
         * Log2(Number of words in cache line)-2
         * 
         * line size is needed in bytes, so number is shifted left
         * by 2 additional bits (4 bytes/word)
         *
         * NOTE: if this is to be core-specific, there may be only a
         * very limited number of cache configurations, so the code
         * could be simplified to check for them
         */

        temp = szInfo & ARMV7_CCSID_LINESIZE_MSK;
        cacheAttrP->lineSize = 1 << (temp + 4);

            /* cache line step = line size */

            cacheAttrP->lineStep = cacheAttrP->lineSize;

        /* decode associativity, read as: (# of ways - 1) */

        cacheAttrP->numWays = ((szInfo & ARMV7_CCSID_WAYNUM_MSK) >>
                   ARMV7_CCSID_WAYNUM_SHFT) + 1;

            cacheAttrP->wayStep = cacheAttrP->lineSize;

            /* decode number of sets, read as: (# of sets - 1) */

            cacheAttrP->numSegs = ((szInfo & ARMV7_CCSID_SETNUM_MSK) >>
                                    ARMV7_CCSID_SETNUM_SHFT) + 1;

            cacheAttrP->segStep = cacheAttrP->lineSize;

        /* 
         * calculate cache size:
         * # sets * # ways * # bytes/line
         */

        cacheAttrP->cacheSize = cacheAttrP->numSegs * 
                    cacheAttrP->numWays *
                    cacheAttrP->lineSize;

#ifdef _WRS_SUPPORT_CACHE_XLATE
            for ( cacheOp_idx = 0; cacheOp_idx < CACHE_OP_LAST;
                  cacheOp_idx++, cacheOpP++ )
                {
                opFlags = cacheOpP->opType & C_FLG_OP_MASK;

                if (opFlags == CACHE_OP_DMAVIRTOPHYS)
                    {
                    cacheOpP->opPtr = cacheAimCortexA9VirtToPhys;
                    }

                if (opFlags == CACHE_OP_DMAPHYSTOVIRT)
                    {
                    cacheOpP->opPtr = cacheAimCortexA9PhysToVirt;
                    }
            }
#endif /* _WRS_SUPPORT_CACHE_XLATE */
            }
        }

    /* Call AIM */

    aimCacheInit (&thisCache);

    /* check for incorrect parameters which are not supported */

    if ((instMode & ICACHE_UNSUPPORT) || (dataMode & DCACHE_UNSUPPORT))
        return (ERROR);

    /*
     * Initialize core-specific function pointers so that the
     * architecture-specific code calls the correct code:
     * - cacheCortexA9DClearDisable disables L1 cache,
     * - cacheAimArch6IClearDisable disables L1 cache.
     */

    _func_cacheAimArch7DClearDisable = cacheCortexA9DClearDisable;
    _func_cacheAimArch7IClearDisable = cacheAimArch6IClearDisable;

#ifndef _WRS_CONFIG_WRHV_GUEST
    /*
     * Turn off and invalidate all caches. This will have been done in hardware
     * on reset, but we may not get here from reset.
     */

    /* Clear & invalidate all D-caches, disable them, and drain Write Buffer. */

    _func_cacheAimArch7DClearDisable ();

    /* Disable and clear (=flush and invalidate) I-cache */

    _func_cacheAimArch7IClearDisable ();

#endif /* !_WRS_CONFIG_WRHV_GUEST */

#ifdef _WRS_CONFIG_SMP
    cacheCortexA9MPCoreSMPInit();
#endif /* _WRS_CONFIG_SMP */

    cacheDataMode   = dataMode;   /* save dataMode for enable */

#ifndef _WRS_CONFIG_WRHV_GUEST

    cacheDataEnabled    =  0 ;    /* D-cache is currently off */

#else

    cacheDataEnabled    =  1 ;    /* D-cache is currently on */

#endif /* !_WRS_CONFIG_WRHV_GUEST */

    cacheMmuAvailable   =  0 ;    /* No MMU yet. This will be */
                                  /* set true by vm(Base)LibInit() */

    /* This insures that memAlign aligns to a cache line boundry */

    if (cacheAttrP != 0)
        cacheArchAlignSize = cacheAttrP->lineSize;
    else
        return (ERROR);  /* cache init. went wrong */

#ifdef _WRS_CONFIG_ENABLE_CACHED_PAGE_TBL
    /* Cortex-A9 uses MPCore specific TTBR & PTE settings */
    mmuArchTtbrFlags = MMU_TTBR_CACHE_PTE_A9_FLAGS;
    mmuArchPteFlags  = (MMU_STATE_CACHE_PTE_A9_FLAGS |
                        MMU_STATE_SUP_RO | MMU_STATE_EXE_NOT);

#  ifdef _WRS_CONFIG_SMP
    mmuArchPteFlags |= MMU_STATE_SHARED;
#  endif /* _WRS_CONFIG_SMP */

#endif /* _WRS_CONFIG_ENABLE_CACHED_PAGE_TBL */

    initialized =  1 ;

    return (OK);
    } /* cacheAimCortexA9LibInit() */

/*******************************************************************************
*
* CLIDRegisterParser - armv7a Cache Level ID Register 
*
* This routine parsers Cache Level ID Register(CLIDR).
* Only used to show info.
*
* RETURNS: void
*
*/

void CLIDRegisterParser(void)
{
    UINT32 val32, ctype;
    int i;

    val32 = cacheArch7CLIDR();

    printf("CLIDR = 0x%08x\r\n", val32);

    for(i=0; i<7; i++)
        {
        printf("\r\nL%d cache: \r\n", i+1);
        
        ctype = (val32>>(i*3)) & 0x7;
        switch(ctype)
            {
            case 0:
                printf("No this cache\r\n");
                break;
            case 1:
                printf("Instruction cache only\r\n");
                break;
            case 2:
                printf("Data cache only\r\n");
                break;
            case 3:
                printf("Separate instruction and data caches\r\n");
                break;
            case 4:
                printf("Unified cache\r\n");
                break;
            default:
                printf("reserved\r\n");
                break;
                
            }
        }
}

/*******************************************************************************
*
* CCSIDRegisterParser - armv7a Cache Size ID Register 
*
* This routine parsers Cache Size ID Register(CCSIDR).
* Only used to show info.
*
* RETURNS: void
*
*/

void CCSIDRegisterParser(UINT32 szInfo)
{
    UINT32 cTypes, temp, temp2;
    UINT32 lineSize, numWays, numSegs, cacheSize;

    printf("  CCSIDR=[0x%08x]\r\n", szInfo);
    
    /* decode cache mode */

    temp = szInfo & ARMV7_CCSID_MODE_MSK;
    
    if (temp & ARMV7_CCSID_MODE_WT)
        printf("  Support: write-though mode\r\n");

    if (temp & ARMV7_CCSID_MODE_WB)
        printf("  Support: write-back mode\r\n");

    if (temp & ARMV7_CCSID_MODE_RA)
        printf("  Support: read-allocate mode\r\n");

    if (temp & ARMV7_CCSID_MODE_WA)
        printf("  Support: write-allocate mode\r\n");
            
    /* 
     * decode cache line size, read as:
     * Log2(Number of words in cache line)-2
     * 
     * line size is needed in bytes, so number is shifted left
     * by 2 additional bits (4 bytes/word)
     *
     * NOTE: If this is to be core-specific, there may be only a
     * very limited number of cache configurations, so the code
     * could be simplified to check for them
     */

    temp = szInfo & ARMV7_CCSID_LINESIZE_MSK;
    lineSize = 1 << (temp + 4);    /*FT200X=> 64*/



    /* decode associativity, read as: (# of ways - 1) 
     * The associativity does not have to be a power of 2.
     */

    numWays = ((szInfo & ARMV7_CCSID_WAYNUM_MSK) >>
               ARMV7_CCSID_WAYNUM_SHFT) + 1;


    /* decode number of sets, read as: (# of sets - 1) 
     * The number of sets does not have to be a power of 2.
     */

     numSegs = ((szInfo & ARMV7_CCSID_SETNUM_MSK) >>
                                ARMV7_CCSID_SETNUM_SHFT) + 1;


    /* 
     * calculate cache size:
     * # sets * # ways * # bytes/line
     */

    cacheSize = numSegs * numWays *lineSize;

    printf("  cache  line: %d (bytes)\r\n", lineSize);
    printf("  number ways: %d \r\n", numWays);
    printf("  number sets: %d \r\n", numSegs);
    printf("  cache  size: %d (bytes) = %d (KB)\r\n", cacheSize, cacheSize/1024);
}

/*******************************************************************************
*
* L1CacheInfo - show L1 cache 
*
* This routine shows L1 cache details.
* Only used to show info.
*
* RETURNS: void
*
*/

void L1CacheInfo(void)
{
    UINT32 szInfo;
    
    szInfo = cacheAimArch7SizeInfoGet (ARMV7_CACHE_LVL1, ARMV7_CSSEL_ICACHE_TYPE);
    printf("----------L1 I-Cache ------ \r\n");
    CCSIDRegisterParser(szInfo);
    
    szInfo =  cacheAimArch7SizeInfoGet (ARMV7_CACHE_LVL1, ARMV7_CSSEL_DCACHE_TYPE);
    printf("----------L1 D-Cache ------ \r\n");
    CCSIDRegisterParser(szInfo);
}


/*******************************************************************************
*
* L1to7CacheInfo - show L1~L7 caches
*
* This routine shows all this chip caches details.
* Only used to show info.
*
* RETURNS: void
*
*/

void L1to7CacheInfo(void)
{
    UINT32 val32, ctype;
    UINT32 szInfo;
    int i;
    
    val32 = cacheArch7CLIDR();
    printf("CLIDR = 0x%08x\r\n", val32);
    
    for(i=0; i<7; i++)
        {
        printf("\r\n------ L%d cache info: ------\r\n", i+1);
        
        ctype = (val32>>(i*3)) & 0x7;
        switch(ctype)
            {
            case 0:
                printf("No this cache\r\n");
                break;
            case 1:
                printf("Instruction cache only\r\n");
                szInfo = cacheAimArch7SizeInfoGet (i, ARMV7_CSSEL_ICACHE_TYPE);
                CCSIDRegisterParser(szInfo);
                break;
            case 2:
                printf("Data cache only\r\n");
                szInfo =  cacheAimArch7SizeInfoGet (i, ARMV7_CSSEL_DCACHE_TYPE);
                CCSIDRegisterParser(szInfo);
                break;
            case 3:
                printf("Separate instruction and data caches\r\n");
                printf("I-Cache info:\r\n");
                szInfo = cacheAimArch7SizeInfoGet (i, ARMV7_CSSEL_ICACHE_TYPE);
                CCSIDRegisterParser(szInfo);
                printf("\r\n");
                printf("D-Cache info:\r\n");
                szInfo =  cacheAimArch7SizeInfoGet (i, ARMV7_CSSEL_DCACHE_TYPE);
                CCSIDRegisterParser(szInfo);
                break;
            case 4:
                printf("Unified cache\r\n");
                szInfo =  cacheAimArch7SizeInfoGet (i, ARMV7_CSSEL_DCACHE_TYPE);
                CCSIDRegisterParser(szInfo);
                break;
            default:
                printf("reserved\r\n");
                break;
            }
        }

    return;
}

