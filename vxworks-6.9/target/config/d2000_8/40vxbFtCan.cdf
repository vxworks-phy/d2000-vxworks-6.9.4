/* 40vxbFtCan.cdf - Can controller configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   DRV_FTCAN {
   NAME        FT CAN controller driver
    SYNOPSIS    FT CAN controller driver
    _CHILDREN   FOLDER_DRIVERS
    CONFIGLETTES 
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    vxbFtCanRegister();
    PROTOTYPE   void vxbFtCanRegister (void);
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS
   INIT_AFTER  INCLUDE_PLB_BUS
}

