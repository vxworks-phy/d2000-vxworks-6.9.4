/* 40vxbYt8521Phy.cdf - yt8521 Phy configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   INCLUDE_YT8521PHY {
	NAME		yt8521 Phy driver
	SYNOPSIS	yt8521 10/100/1000 ethernet PHY
	_CHILDREN	FOLDER_DRIVERS
	_INIT_ORDER	hardWareInterFaceBusInit
	INIT_RTN	ytPhyRegister();
	REQUIRES	INCLUDE_MII_BUS
	INIT_AFTER	INCLUDE_MII_BUS
	CFG_PARAMS  MII_MODE_SET_AUTONEG_FORCE
	CONFIGLETTES usrMiiCfg.c
}

Parameter MII_MODE_SET_AUTONEG_FORCE {
        NAME            Force Auto-Negotiation
        SYNOPSIS        Force Auto-Negotiation for all modes in miiModeSet()
        TYPE            BOOL
        DEFAULT         TRUE
}
