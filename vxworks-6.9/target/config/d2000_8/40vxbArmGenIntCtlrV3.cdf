/* 40vxbArmGenIntCtlrV3.cdf - ARM GIC Version3 interrupt controller */
 
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 

 
Component DRV_ARM_GICV3 {
        NAME            ARM Generic Version3 Interrupt Controller driver
        SYNOPSIS        ARM Generic Version3 Interrupt Controller driver
        REQUIRES        INCLUDE_VXBUS \
                        INCLUDE_PLB_BUS \
                        INCLUDE_INTCTLR_LIB
        _CHILDREN       FOLDER_DRIVERS
        INIT_RTN        vxbFTIntCtlrRegister();
        PROTOTYPE       void vxbFTIntCtlrRegister (void);
        INIT_AFTER      INCLUDE_PLB_BUS
        _INIT_ORDER     hardWareInterFaceBusInit
}

