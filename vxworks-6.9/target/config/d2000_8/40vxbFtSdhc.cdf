/* 40vxbFtSdhc.cdf - SD controller configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   INCLUDE_FT_SD {
   NAME        FT SDHC host controller driver
    SYNOPSIS    FT SDHC host controller driver
    _CHILDREN   FOLDER_DRIVERS
    CONFIGLETTES usrSdMmc.c
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    ftSdhcRegister();\
                 usrSdMmcInit();
    PROTOTYPE   void ftSdhcRegister (void);
    REQUIRES    DRV_SDSTORAGE_CARD\
                INCLUDE_VXBUS \
                INCLUDE_PLB_BUS \
                INCLUDE_ERF \
                INCLUDE_SD_BUS
   INIT_AFTER  INCLUDE_PLB_BUS  \
                INCLUDE_SD_BUS 
}

