/* miiBus.h - MII bus controller and API library */

/* Copyright (c) 2005-2008, 2012, 2013, 2015 Wind River Systems, Inc. */

/*
modification history
--------------------
24jul15,d_l  add flow control advertise ability support. (VXW6-84701)
31dec13,xms  add  miiBusLpiModeGet routine. (WIND00440439)
01g,10jan12,rec  WIND00326681 - miiBusMonitor task polling inhibits power
                 management.  Add miiBusQuiesce and miiBusWakeup.
01f,04sep08,wap  Add miiBusIdleErrorCheck() routine (WIND00129165)
01e,29mar07,tor  update methods
01d,05sep06,kch  Replaced if_media.h include with endMedia.h.
01c,20jun06,wap  Remove prototype for vxbNextUnitGet
01b,25may06,wap  Add support for miibus device deletion
01a,04dec05,wap  Written.
*/

#ifndef __INCmiiBush
#define __INCmiiBush

#include <lstLib.h>
#include <miiLib.h>
#include <endLib.h>
#include <endMedia.h>
#include <vxBusLib.h>

#define IDR2_OUILSB     0xfc00  /* OUI LSB */
#define IDR2_MODEL      0x03f0  /* vendor model */
#define IDR2_REV        0x000f  /* vendor revision */

#define MII_OUI(id1, id2)       (((id1) << 6) | ((id2) >> 10))
#define MII_MODEL(id2)          (((id2) & IDR2_MODEL) >> 4)
#define MII_REV(id2)            ((id2) & IDR2_REV)

/* Flow control ability */

#define MII_FCADV_NONE      (0)
#define MII_FCADV_PAUSE     (1)
#define MII_FCADV_ASM       (2)
#define MII_FCADV_PAUSE_ASM (MII_FCADV_PAUSE|MII_FCADV_ASM)
#define MII_ANAR_PAUSE_SHIFT    (10)

IMPORT void miiBusRegister(void);

IMPORT STATUS miiBusCreate(VXB_DEVICE_ID, VXB_DEVICE_ID *);
IMPORT STATUS miiBusDelete(VXB_DEVICE_ID);
IMPORT STATUS miiBusGet(VXB_DEVICE_ID, VXB_DEVICE_ID *);
IMPORT STATUS miiBusRead(VXB_DEVICE_ID, int, int, UINT16 *);
IMPORT STATUS miiBusWrite(VXB_DEVICE_ID, int, int, UINT16);
IMPORT STATUS miiBusLpiModeGet(VXB_DEVICE_ID, UINT16 *);
IMPORT STATUS miiBusModeGet(VXB_DEVICE_ID, UINT32 *, UINT32 *);
IMPORT STATUS miiBusModeSet(VXB_DEVICE_ID, UINT32);
IMPORT STATUS miiBusMediaUpdate(VXB_DEVICE_ID);
IMPORT STATUS miiBusMediaAdd(VXB_DEVICE_ID, UINT32);
IMPORT STATUS miiBusMediaDel(VXB_DEVICE_ID, UINT32);
IMPORT STATUS miiBusMediaDefaultSet(VXB_DEVICE_ID, UINT32);
IMPORT STATUS miiBusMediaListGet(VXB_DEVICE_ID, END_MEDIALIST **);
IMPORT void miiBusListAdd(VXB_DEVICE_ID);
IMPORT void miiBusListDel(VXB_DEVICE_ID);
IMPORT STATUS miiBusIdleErrorCheck(VXB_DEVICE_ID);
#ifdef _WRS_CONFIG_PWR_MGMT
IMPORT STATUS miiBusQuiesce (void);
IMPORT void miiBusWakeup (void);
IMPORT STATUS miiBusStateSet(UINT32 state, void * context);
#endif /* _WRS_CONFIG_PWR_MGMT  */

typedef struct mii_drv_ctrl
    {
    NODE        miiNode;
    VXB_DEVICE_ID   miiSelf;
    VXB_DEVICE_ID   miiParent;
    VXB_DEVICE_ID   miiActivePhy;
    int         miiPhyCnt;
    VXB_DEVICE_ID   miiPhyList[32];
    BOOL        miiInitialized;
    int         miiPhyAddr;
    UINT16      miiId1;
    UINT16      miiId2;
    UINT16      miiSts;
    BOOL        miiLeaving;
    END_MEDIALIST   *miiMediaList;
    UINT16      fcmode;
    } MII_DRV_CTRL;

#endif /* __INCmiiBush */
